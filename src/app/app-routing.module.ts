import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppDownloadPageComponent } from './components/pages/app-download-page/app-download-page.component';
import { ComingSoonPageComponent } from './components/pages/coming-soon-page/coming-soon-page.component';
import { ContactPageComponent } from './components/pages/contact-page/contact-page.component';
import { FaqPageComponent } from './components/pages/faq-page/faq-page.component';
import { HowItWorksPageComponent } from './components/pages/how-it-works-page/how-it-works-page.component';
import { NotFoundComponent } from './components/pages/not-found/not-found.component';

//Ai-octopus menu
import { HomeComponent } from './components/pages/home/home.component';
import { OctopusBenefitsComponent } from './components/pages/octopus-benefits/octopus-benefits.component';
import { OctopusFeaturesComponent } from './components/pages/octopus-features/octopus-features.component';
import { OctopusPricingComponent } from './components/pages/octopus-pricing/octopus-pricing.component';
import { OctopusClientsComponent } from './components/pages/octopus-clients/octopus-clients.component';

import { OctopusContactSalesComponent } from './components/pages/octopus-contact-sales/octopus-contact-sales.component';

const routes: Routes = [
    { path: '', component: HomeComponent },
    { path: 'benefits', component: OctopusBenefitsComponent },
    { path: 'features', component: OctopusFeaturesComponent },
    { path: 'pricing', component: OctopusPricingComponent },
    { path: 'clients', component: OctopusClientsComponent },
    { path: 'contact-sales', component: OctopusContactSalesComponent },

    { path: 'faq', component: FaqPageComponent },
    { path: 'how-it-works', component: HowItWorksPageComponent },
    { path: 'coming-soon', component: ComingSoonPageComponent },
    { path: 'app-download', component: AppDownloadPageComponent },
    { path: 'contact', component: ContactPageComponent },
    // Here add new pages component

    { path: '**', component: NotFoundComponent } // This line will remain down from the whole pages component list
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }