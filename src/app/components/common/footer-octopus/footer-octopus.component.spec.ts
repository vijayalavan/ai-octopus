import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FooterOctopusComponent } from './footer-octopus.component';

describe('FooterOctopusComponent', () => {
  let component: FooterOctopusComponent;
  let fixture: ComponentFixture<FooterOctopusComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FooterOctopusComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FooterOctopusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
