import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BenefitsOneComponent } from './benefits-one.component';

describe('BenefitsOneComponent', () => {
  let component: BenefitsOneComponent;
  let fixture: ComponentFixture<BenefitsOneComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BenefitsOneComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BenefitsOneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
