import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OctopusBenefitsComponent } from './octopus-benefits.component';

describe('OctopusBenefitsComponent', () => {
  let component: OctopusBenefitsComponent;
  let fixture: ComponentFixture<OctopusBenefitsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OctopusBenefitsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OctopusBenefitsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
