import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BenefitsFiveComponent } from './benefits-five.component';

describe('BenefitsFiveComponent', () => {
  let component: BenefitsFiveComponent;
  let fixture: ComponentFixture<BenefitsFiveComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BenefitsFiveComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BenefitsFiveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
