import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BenefitsTwoComponent } from './benefits-two.component';

describe('BenefitsTwoComponent', () => {
  let component: BenefitsTwoComponent;
  let fixture: ComponentFixture<BenefitsTwoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BenefitsTwoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BenefitsTwoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
