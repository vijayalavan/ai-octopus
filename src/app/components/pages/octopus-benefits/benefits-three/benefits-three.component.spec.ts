import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BenefitsThreeComponent } from './benefits-three.component';

describe('BenefitsThreeComponent', () => {
  let component: BenefitsThreeComponent;
  let fixture: ComponentFixture<BenefitsThreeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BenefitsThreeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BenefitsThreeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
