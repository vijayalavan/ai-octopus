import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BenefitsFourComponent } from './benefits-four.component';

describe('BenefitsFourComponent', () => {
  let component: BenefitsFourComponent;
  let fixture: ComponentFixture<BenefitsFourComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BenefitsFourComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BenefitsFourComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
