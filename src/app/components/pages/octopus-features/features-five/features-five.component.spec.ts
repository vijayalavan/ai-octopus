import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FeaturesFiveComponent } from './features-five.component';

describe('FeaturesFiveComponent', () => {
  let component: FeaturesFiveComponent;
  let fixture: ComponentFixture<FeaturesFiveComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FeaturesFiveComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FeaturesFiveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
