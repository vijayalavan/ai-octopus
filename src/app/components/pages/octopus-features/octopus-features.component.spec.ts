import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OctopusFeaturesComponent } from './octopus-features.component';

describe('OctopusFeaturesComponent', () => {
  let component: OctopusFeaturesComponent;
  let fixture: ComponentFixture<OctopusFeaturesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OctopusFeaturesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OctopusFeaturesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
