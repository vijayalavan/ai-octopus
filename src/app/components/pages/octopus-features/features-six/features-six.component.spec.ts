import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FeaturesSixComponent } from './features-six.component';

describe('FeaturesSixComponent', () => {
  let component: FeaturesSixComponent;
  let fixture: ComponentFixture<FeaturesSixComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FeaturesSixComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FeaturesSixComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
