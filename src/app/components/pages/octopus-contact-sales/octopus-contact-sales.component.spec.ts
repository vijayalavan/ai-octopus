import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OctopusContactSalesComponent } from './octopus-contact-sales.component';

describe('OctopusContactSalesComponent', () => {
  let component: OctopusContactSalesComponent;
  let fixture: ComponentFixture<OctopusContactSalesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OctopusContactSalesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OctopusContactSalesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
