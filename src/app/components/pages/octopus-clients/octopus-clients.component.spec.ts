import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OctopusClientsComponent } from './octopus-clients.component';

describe('OctopusClientsComponent', () => {
  let component: OctopusClientsComponent;
  let fixture: ComponentFixture<OctopusClientsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OctopusClientsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OctopusClientsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
