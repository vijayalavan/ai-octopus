import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeAboutOctopusComponent } from './home-about-octopus.component';

describe('HomeAboutOctopusComponent', () => {
  let component: HomeAboutOctopusComponent;
  let fixture: ComponentFixture<HomeAboutOctopusComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HomeAboutOctopusComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeAboutOctopusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
