import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ShotClientsComponent } from './shot-clients.component';

describe('ShotClientsComponent', () => {
  let component: ShotClientsComponent;
  let fixture: ComponentFixture<ShotClientsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ShotClientsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ShotClientsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
