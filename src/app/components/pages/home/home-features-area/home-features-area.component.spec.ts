import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeFeaturesAreaComponent } from './home-features-area.component';

describe('HomeFeaturesAreaComponent', () => {
  let component: HomeFeaturesAreaComponent;
  let fixture: ComponentFixture<HomeFeaturesAreaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HomeFeaturesAreaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeFeaturesAreaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
