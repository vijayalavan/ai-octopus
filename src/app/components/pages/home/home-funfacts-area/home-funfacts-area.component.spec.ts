import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeFunfactsAreaComponent } from './home-funfacts-area.component';

describe('HomeFunfactsAreaComponent', () => {
  let component: HomeFunfactsAreaComponent;
  let fixture: ComponentFixture<HomeFunfactsAreaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HomeFunfactsAreaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeFunfactsAreaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
