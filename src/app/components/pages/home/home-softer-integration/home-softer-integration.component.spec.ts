import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeSofterIntegrationComponent } from './home-softer-integration.component';

describe('HomeSofterIntegrationComponent', () => {
  let component: HomeSofterIntegrationComponent;
  let fixture: ComponentFixture<HomeSofterIntegrationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HomeSofterIntegrationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeSofterIntegrationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
