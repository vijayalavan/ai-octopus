import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeAppIntegrationComponent } from './home-app-integration.component';

describe('HomeAppIntegrationComponent', () => {
  let component: HomeAppIntegrationComponent;
  let fixture: ComponentFixture<HomeAppIntegrationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HomeAppIntegrationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeAppIntegrationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
