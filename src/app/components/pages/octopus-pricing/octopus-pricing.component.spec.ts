import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OctopusPricingComponent } from './octopus-pricing.component';

describe('OctopusPricingComponent', () => {
  let component: OctopusPricingComponent;
  let fixture: ComponentFixture<OctopusPricingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OctopusPricingComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OctopusPricingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
